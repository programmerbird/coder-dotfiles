#!/bin/bash


if ! [ -x "$(command -v rake)" ]; then
    sudo apt update 
    sudo apt install -y rake
fi

if ! [ -x "$(command -v pip)" ]; then
    sudo apt update 
    sudo apt install -y python3-pip
fi

if ! [ -x "$(command -v fab)" ]; then
    sudo apt update 
    sudo pip install requests fabric3
fi
